import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.util.*;
import java.net.*;
/**
 *
 * @author user
 */
public class NET_INF{
    
    public NetworkInterface NETINT;
    Enumeration<NetworkInterface> All_INTERFACES;
    Enumeration<InetAddress> All_ADDRESSES;
    public int InterfaceCount = 0;
    public int InterfaceNumber =0;
       
        private JFrame MainWindow=new JFrame("Network Info");
        private JLabel L_Title = new JLabel("Network Information");
        private JLabel L_TotalInterfaces = new JLabel("Total #interfaces");
        private JLabel L_TotalInterfaces_Box = new JLabel();
        private JLabel L_InterfaceNumber = new JLabel("Network interface info");
        private JLabel L_InterfaceNumber_Box = new JLabel();
        private JLabel L_InterfaceName = new JLabel("Interface name");
        private JLabel L_InterfaceName_Box = new JLabel();        
        private JLabel L_InterfaceID = new JLabel("Interface ID");
        private JLabel L_InterfaceID_Box = new JLabel();
        private JLabel L_MAC = new JLabel("MAC address");
        private JLabel L_MAC_Box = new JLabel();
        private JLabel L_IP = new JLabel("IP address");
        private JLabel L_IP_Box = new JLabel();
        private JLabel L_HostName = new JLabel("Host name");
        private JLabel L_HostName_Box = new JLabel();
        private JLabel L_MTU = new JLabel("MTU");
        private JLabel L_MTU_Box = new JLabel();
        private JLabel L_Status = new JLabel("Status");
        private JLabel L_Status_Box = new JLabel();
        private JLabel L_PointToPoint = new JLabel("Point to point");
        private JLabel L_PointToPoint_Box = new JLabel();
        private JLabel L_Multicast = new JLabel("Multicast");
        private JLabel L_Multicast_Box = new JLabel();
        private JLabel L_Loopback = new JLabel("Loopback");
        private JLabel L_Loopback_Box = new JLabel();
        private JLabel L_Virtual = new JLabel("Virtual");
        private JLabel L_Virtual_Box = new JLabel();
        private JButton B_NEXT = new JButton("NEXT");
        private JButton B_PREVIOUS= new JButton("PREV");
        private JButton B_QUIT = new JButton("QUIT");
        
        public static void main(String arg[])
        {
            new NET_INF();
        }
        
        public NET_INF() {
        BuildGUI();
        GetInterfaces();
        DisplayInterfaceInfo(InterfaceNumber);

    }
    
        public void GetInterfaces() {
        try
        {
            All_INTERFACES = NetworkInterface.getNetworkInterfaces();
            InterfaceCount = Collections.list(All_INTERFACES).size();
        }
        catch(SocketException X)
        {System.out.println(X);}
    }   
      
        public void DisplayInterfaceInfo(int INT_NUM) {
        try{
            All_INTERFACES = NetworkInterface.getNetworkInterfaces();
            NETINT = Collections.list(All_INTERFACES).get(INT_NUM);
            L_TotalInterfaces_Box.setText(Integer.toString(InterfaceCount));
            L_InterfaceNumber_Box.setText(Integer.toString(INT_NUM+1));
            L_InterfaceName_Box.setText(NETINT.getDisplayName());
            L_InterfaceID_Box.setText(NETINT.getName());
            L_MAC_Box.setText(Arrays.toString(NETINT.getHardwareAddress()));
            All_ADDRESSES=NETINT.getInetAddresses();
            for(InetAddress X:Collections.list(All_ADDRESSES))
            {
                L_IP_Box.setText(X.getHostAddress());
                L_HostName_Box.setText(X.getHostName());
            }
            L_MTU_Box.setText(Integer.toString(NETINT.getMTU()));
            String STATUS;
            if(NETINT.isUp()){STATUS="Up!";}
            else{STATUS="Down!";}
            L_Status_Box.setText(STATUS);
            
            
            String POINTTOPOINT;
            if(NETINT.isPointToPoint()){POINTTOPOINT="Yes!";}
            else{POINTTOPOINT="No!";}
            L_PointToPoint_Box.setText(POINTTOPOINT);
            
            String MULTICAST;
            if(NETINT.supportsMulticast()){MULTICAST="Yes!";}
            else{MULTICAST="No!";}
            L_Multicast_Box.setText(MULTICAST);
            
            String LOOPBACK;
            if(NETINT.isLoopback()){LOOPBACK="Yes!";}
            else{LOOPBACK="No!";}
            L_Loopback_Box.setText(LOOPBACK);
            
            String ISVIRTUAL;
            if(NETINT.isVirtual()){ISVIRTUAL="Yes!";}
            else{ISVIRTUAL="No!";}
            L_Virtual_Box.setText(ISVIRTUAL);
        }
         catch(SocketException X)
         {System.out.print(X);}
            }

    public void BuildGUI() {
        MainWindow.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        MainWindow.setSize(800,800);
        MainWindow.setLayout(null);
        //--------------------------------------------------------
        MainWindow.add(L_Title);
        L_Title.setBounds(112, 0, 172, 16);
        //--------------------------------------------------------
        MainWindow.add(L_TotalInterfaces);
        L_TotalInterfaces.setBounds(12, 23, 181, 16);
        
        L_TotalInterfaces_Box.setForeground(new java.awt.Color(255,0,0));
        L_TotalInterfaces_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_TotalInterfaces_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add(L_TotalInterfaces_Box);
        L_TotalInterfaces_Box.setBounds(250, 23, 200, 16);
        //---------------------------------------------------------
        MainWindow.add(L_InterfaceNumber);
        L_InterfaceNumber.setBounds(12, 46, 136, 16);
        
        L_InterfaceNumber_Box.setForeground(new java.awt.Color(255,0,0));
        L_InterfaceNumber_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_InterfaceNumber_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_InterfaceNumber_Box);
        L_InterfaceNumber_Box.setBounds(250, 46, 200, 16);
        
        //-----------------------------------------------------------
        MainWindow.add(L_InterfaceName);
        L_InterfaceName.setBounds(12, 76, 136, 16);
        
        L_InterfaceName_Box.setForeground(new java.awt.Color(255,0,0));
        L_InterfaceName_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_InterfaceName_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_InterfaceName_Box);
        L_InterfaceName_Box.setBounds(250, 76, 200, 30);
        
        //-----------------------------------------------------------
        MainWindow.add(L_InterfaceID);
        L_InterfaceID.setBounds(12, 106, 136, 16);
        
        L_InterfaceID_Box.setForeground(new java.awt.Color(255,0,0));
        L_InterfaceID_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_InterfaceID_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_InterfaceID_Box);
        L_InterfaceID_Box.setBounds(250, 106, 200, 30);
       
        //-------------------------------------------------------------
        MainWindow.add(L_MAC);
        L_MAC.setBounds(12, 330, 136, 16);
        
        L_MAC_Box.setForeground(new java.awt.Color(255,0,0));
        L_MAC_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_MAC_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_MAC_Box);
        L_MAC_Box.setBounds(250, 330, 200, 30);
        
        //-------------------------------------------------------------
        
        MainWindow.add(L_IP);
        L_IP.setBounds(12, 200, 136, 16);
        
        L_IP_Box.setForeground(new java.awt.Color(255,0,0));
        L_IP_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_IP_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_IP_Box);
        L_IP_Box.setBounds(250, 161, 200, 30);
        
        //-----------------------------------------------------------
        MainWindow.add(L_HostName);
        L_HostName.setBounds(12, 190, 100, 16);

        L_HostName_Box.setForeground(new java.awt.Color(255,0,0));
        L_HostName_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_HostName_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_HostName_Box);
        L_HostName_Box.setBounds(250, 190, 200, 30);

        //-----------------------------------------------------------
        MainWindow.add(L_MTU);
        L_MTU.setBounds(12, 210, 136, 16);
        
        L_MTU_Box.setForeground(new java.awt.Color(255,0,0));
        L_MTU_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_MTU_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_MTU_Box);
        L_MTU_Box.setBounds(250, 210, 200, 30);
       
        //-----------------------------------------------------------
        MainWindow.add(L_Status);
        L_Status.setBounds(12, 230, 136, 16);
        
        L_Status_Box.setForeground(new java.awt.Color(255,0,0));
        L_Status_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_Status_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_Status_Box);
        L_Status_Box.setBounds(250, 230, 200, 30);
       
        //-----------------------------------------------------------
        MainWindow.add(L_PointToPoint);
        L_PointToPoint.setBounds(12, 250, 136, 16);
        
        L_PointToPoint_Box.setForeground(new java.awt.Color(255,0,0));
        L_PointToPoint_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_PointToPoint_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_PointToPoint_Box);
        L_PointToPoint_Box.setBounds(250, 250, 200, 30);
       
        //-----------------------------------------------------------
        MainWindow.add(L_Multicast);
        L_Multicast.setBounds(12, 270, 136, 16);
        
        L_Multicast_Box.setForeground(new java.awt.Color(255,0,0));
        L_Multicast_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_Multicast_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_Multicast_Box);
        L_Multicast_Box.setBounds(250, 270, 200, 30);
       
        //-----------------------------------------------------------
        MainWindow.add(L_Loopback);
        L_Loopback.setBounds(12, 290, 136, 16);
        
        L_Loopback_Box.setForeground(new java.awt.Color(255,0,0));
        L_Loopback_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_Loopback_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_Loopback_Box);
        L_Loopback_Box.setBounds(250, 290, 200, 30);
       
        //-----------------------------------------------------------
        MainWindow.add(L_Virtual);
        L_Virtual.setBounds(12, 310, 136, 16);
        
        L_Virtual_Box.setForeground(new java.awt.Color(255,0,0));
        L_Virtual_Box.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        L_Virtual_Box.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MainWindow.add( L_Virtual_Box);
        L_Virtual_Box.setBounds(250, 310, 200, 30);
      //---------------------------------------------------------------
      
      MainWindow.add(B_NEXT);
      B_NEXT.setBounds(12, 360, 136, 16);
        
        B_NEXT.setForeground(new java.awt.Color(255,0,0));
        B_NEXT.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        B_NEXT.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

      MainWindow.add(B_PREVIOUS);
      B_PREVIOUS.setBounds(12, 380, 136, 16);
        
        B_PREVIOUS.setForeground(new java.awt.Color(255,0,0));
        B_PREVIOUS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        B_PREVIOUS.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        
      MainWindow.add(B_QUIT);
      B_QUIT.setBounds(12, 400, 136, 16);
        
        B_QUIT.setForeground(new java.awt.Color(255,0,0));
        B_QUIT.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        B_QUIT.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

                Add_Listeners();
         MainWindow.setVisible(true);
    }

    public void Add_Listeners() {
                        
                B_PREVIOUS.addActionListener(
                new java.awt.event.ActionListener() {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        {ACTION_B_PREVIOUS();}
                    }
                }
            );
                
           B_NEXT.addActionListener(
                new java.awt.event.ActionListener() {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        {ACTION_B_NEXT();}
                    }
                }
            );
           
           B_QUIT.addActionListener(
                new java.awt.event.ActionListener() {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        {ACTION_B_QUIT();}
                    }
                }
            );           
                
    }
    
    public void ACTION_B_PREVIOUS(){
            if(InterfaceNumber>0)
        {
            InterfaceNumber--;
            DisplayInterfaceInfo(InterfaceNumber);
        }
    
    }
    public void ACTION_B_NEXT(){
        if(InterfaceNumber<(InterfaceCount-1))
        {
            InterfaceNumber++;
            DisplayInterfaceInfo(InterfaceNumber);
        }
    }
    public void ACTION_B_QUIT(){
        MainWindow.setVisible(false);
        MainWindow.dispose();
    }
}
    
     