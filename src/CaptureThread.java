import javax.swing.SwingUtilities;

public abstract class CaptureThread 
{
        
private Object value;

        private Thread thread;
        private static class ThreadVar
        {
            private Thread thread;
            ThreadVar(Thread t)
            {
                thread =t;
                
            }
            synchronized Thread get(){
                return thread;
            }
                    synchronized void clear()
                            {
                                thread=null;
                            }
                    
                    
        }
        private ThreadVar threadVar;
        protected synchronized Object getValue()
        {
            return value;
            
        }
        private synchronized void setValue(Object x)
        {
         value=x;   
        }
        public abstract Object Construct();
        public void Finished()
        {
            
        }
       public void interrupt()
       {
           Thread ta=threadVar.get();
           if(ta!=null)
           {
               ta.interrupt();
           }
           threadVar.clear();
       
       }
       public Object get()
               {
                while(true){
                    Thread ta=threadVar.get();
                    if(ta==null)
                    {
                        return getValue();
                    }
                    try{
                        
                        ta.join();
                    }catch(InterruptedException ex)
                    {
                        Thread.currentThread().interrupt();
                        return null;
                        
                    }
                }   
               }
            public CaptureThread()
            {
                final Runnable dofinished=new Runnable()
                {
                    public void run(){Finished();}
                        
                    
                };
                Runnable doConstruct=new Runnable()
                {
                    public void run()
                    {
                        try{
                            setValue(Construct());
                        }finally{threadVar.clear();}
                        SwingUtilities.invokeLater(dofinished);
                        
                                
                    }
                };
                Thread ta=new Thread(doConstruct);
                threadVar=new ThreadVar(ta);
                  
            }
       public void start()
       {
       
       Thread ta= threadVar.get();
       if(ta!=null)
       {
           ta.start();
       }
       }
    
}
